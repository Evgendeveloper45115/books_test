<?php

namespace app\models\search;

use app\models\UserHasBook;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Book;
use yii\db\ActiveQuery;
use yii\helpers\VarDumper;

/**
 * BookSearch represents the model behind the search form of `app\models\Book`.
 */
class BookSearch extends Book
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['title', 'code', 'date_create', 'author', 'stock'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $controller = \Yii::$app->controller->id;
        $query = Book::find();
        if ($controller === 'user') {
            $query->joinWith(['userBooks' => function (ActiveQuery $query) {
                $query->andWhere(['change_status' => UserHasBook::STATUS_GET]);
            }]);

        }
        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_create' => $this->date_create,
        ]);
        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'author', $this->author])
            ->andFilterWhere(['=', 'stock', $this->stock]);

        return $dataProvider;
    }
}
