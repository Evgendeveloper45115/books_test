<?php

namespace app\models\search;

use app\models\UserHasBook;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;
use yii\db\ActiveQuery;
use yii\helpers\VarDumper;

/**
 * UserSearch represents the model behind the search form of `app\models\User`.
 */
class UserSearch extends User
{
    public $have_book;

    /**
     * {@inheritdoc}
     */

    public function rules()
    {
        return [
            [['id', 'role', 'position', 'p_series', 'p_number'], 'integer'],
            [['first_name', 'last_name', 'patronymic', 'email', 'password', 'date_create', 'have_book'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();
        if ($this->getBooks()) {
            $query->joinWith(['books' => function (ActiveQuery $query) {
                $query->andWhere(['change_status' => UserHasBook::STATUS_GET]);
            }]);
        }

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'role' => $this->role,
            'position' => $this->position,
            'p_series' => $this->p_series,
            'p_number' => $this->p_number,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'patronymic', $this->patronymic])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'password', $this->password]);

        return $dataProvider;
    }

    public function getBooks()
    {
        return \Yii::$app->request->get('UserSearch')['have_book'] == 1;
    }
}
