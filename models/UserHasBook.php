<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "userHasBook".
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $book_id
 * @property int|null $change_status
 * @property int|null $clean_status
 * @property string|null $change_date
 * @property string|null $date_continue
 */
class UserHasBook extends \yii\db\ActiveRecord
{
    const STATUS_GET = 1;
    const STATUS_TAKE = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'userHasBook';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'book_id', 'change_status', 'clean_status'], 'default', 'value' => null],
            [['user_id', 'book_id', 'change_status', 'clean_status'], 'integer'],
            [['change_date', 'date_continue'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Сотрудник',
            'book_id' => 'Книга',
            'change_status' => 'Выдал/вернул',
            'clean_status' => 'Состояние книги',
            'change_date' => 'Дата - выдал/вернул',
            'date_continue' => 'Срок выдачи',
        ];
    }
}
