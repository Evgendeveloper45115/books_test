<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bookType".
 *
 * @property int $id
 * @property string|null $type
 */
class BookType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bookType';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Состояние книги',
        ];
    }
}
