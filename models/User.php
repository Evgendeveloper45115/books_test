<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property integer $role
 * @property integer $position
 * @property integer $p_series
 * @property integer $p_number
 * @property string $first_name
 * @property string $last_name
 * @property string $patronymic
 * @property string $email
 * @property string $password
 * @property datetime $date_create
 * @property UserHasBook[] $books
 */
class User extends ActiveRecord implements \yii\web\IdentityInterface
{

    const ROLE_USER = 0;
    const ROLE_ADMIN = 1;
    const ROLE_STAFF = 2;

    const POSITION_TRADER = 1;
    const POSITION_OPT = 2;
    const POSITION_SELLER = 3;


    public static $roles = [
        self::ROLE_USER => 'Клиент',
        self::ROLE_STAFF => 'Сотрудник',
    ];

    public static $positions = [
        self::POSITION_TRADER => 'Торговец',
        self::POSITION_OPT => 'Оптовик',
        self::POSITION_SELLER => 'Продавец',
    ];

    /**
     * {@inheritdoc}
     */

    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'role', 'position', 'p_series', 'p_number'], 'integer'],
            [['first_name', 'last_name', 'patronymic', 'email', 'password', 'date_create'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'patronymic' => 'Отчество',
            'role' => 'Роль',
            'p_series' => 'Серия паспорта',
            'p_number' => 'Номер паспорта',
            'password' => 'Пароль',
            'date_create' => 'Дата создания',
            'position' => 'Должность'
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * @param $email
     * @return User|null
     */

    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    public function getBooks()
    {
        return $this->hasMany(UserHasBook::class, ['user_id' => 'id'])->andWhere(['change_status' => UserHasBook::STATUS_GET]);
    }

}
