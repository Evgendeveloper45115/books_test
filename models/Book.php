<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "book".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $code
 * @property string|null $date_create
 * @property string|null $author
 * @property string|null $stock
 * @property UserHasBook $userBooks
 */
class Book extends \yii\db\ActiveRecord
{
    public static $stock = [
        0 => 'Нет на складе',
        1 => 'В наличии',
    ];


    /**
     * {@inheritdoc}
     */

    public static function tableName()
    {
        return 'book';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_create', 'stock'], 'safe'],
            [['title', 'code', 'author'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'code' => 'Артикул',
            'date_create' => 'Дата поступления',
            'author' => 'Автор',
            'stock' => 'Наличие',
        ];
    }

    public function getUserBooks()
    {
        return $this->hasOne(UserHasBook::class, ['book_id' => 'id'])->andWhere(['user_id' => Yii::$app->request->get('id')]);
    }

}
