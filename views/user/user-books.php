<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $books \app\models\Book[] */
/* @var $cleanStatuses [] */
/* @var $UHB \app\models\UserHasBook */
/* @var $form yii\widgets\ActiveForm */
/* @var $searchModel app\models\search\BookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Книги клиента: ' . $model->email;

$this->params['breadcrumbs'][] = ['label' => 'Клиенты/сотрудники', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php \yii\widgets\Pjax::begin() ?>
    <div class="user-form">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($UHB, 'user_id')->hiddenInput(['maxlength' => true, 'value' => $model->id])->label(false) ?>
        <?= $form->field($UHB, 'change_status')->hiddenInput(['maxlength' => true, 'value' => \app\models\UserHasBook::STATUS_GET])->label(false) ?>
        <?= $form->field($UHB, 'book_id')->dropDownList($books) ?>
        <?= $form->field($UHB, 'clean_status')->dropDownList($cleanStatuses) ?>

        <?= '<label class="control-label">Дата окончания</label>' ?>
        <?= \kartik\datetime\DateTimePicker::widget([
            'model' => $UHB,
            'attribute' => 'date_continue',
            'options' => ['placeholder' => 'Выбрать дату окончания'],
            //  'convertFormat' => true,
            'pluginOptions' => [
                'autoclose' => true,
            ]
        ]);
        ?>

        <div class="form-group">
            <?= Html::submitButton('Добавить', ['class' => 'btn btn-success', 'style' => 'margin-top:1vw']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

    <?= \kartik\grid\GridView::widget([
        'id' => 'user-index',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
            [
                'attribute' => '',
                'label' => 'Дата возврата',
                'value' => function(\app\models\Book $model){
                    return $model->userBooks->date_continue;
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '<span style="text-wrap: none">{remove}</span>',
                'header' => 'Действия',
                'buttons' => [
                    'remove' => function ($url, \app\models\Book $model) use ($cleanStatuses) {
                        return Html::dropDownList('remove-book', null, $cleanStatuses, ['class' => 'remove-book']) . Html::button('Вернуть', ['data-book-id' => $model->id, 'data-user-id' => $_GET['id'], 'class' => 'js-take-button']);
                    },
                ]

            ],
        ], // check the configuration for grid columns by clicking button above
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => false, // pjax is set to always true for this demo
        // set your toolbar
        'toggleDataContainer' => ['class' => 'btn-group mr-2'],
        // set export properties
        'export' => [
            'fontAwesome' => true
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'pjaxSettings' => [
            'options' => [
                'timeout' => '50000'
            ]
        ],
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'panel' => [
            'type' => \kartik\grid\GridView::TYPE_PRIMARY,
            'heading' => 'Список Книг',
            'before' => '',
            'after' => false,
            'showFooter' => false
        ],
        'persistResize' => false,
        'toggleDataOptions' => ['minCount' => 1],
        'itemLabelSingle' => 'Книга',
        'itemLabelPlural' => 'Книг'
    ]) ?>
    <?php
    \yii\widgets\Pjax::end()
    ?>

</div>


