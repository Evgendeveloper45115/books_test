<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'patronymic')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'role')->dropDownList(['' => 'Выбрать роль'] + User::$roles, ['class' => 'form-control choose-role-js']) ?>

    <?= $form->field($model, 'position')->dropDownList(['' => 'Выбрать должность'] + User::$positions, ['data-role' => User::ROLE_STAFF, 'class' => 'form-control hidden_input', 'style' => 'display:none'])->label(null, ['class' => 'control-label hidden_label']) ?>

    <?= $form->field($model, 'p_series')->hiddenInput(['data-role' => User::ROLE_USER, 'class' => 'form-control hidden_input'])->label(null, ['class' => 'control-label hidden_label']) ?>

    <?= $form->field($model, 'p_number')->hiddenInput(['data-role' => User::ROLE_USER, 'class' => 'form-control hidden_input'])->label(null, ['class' => 'control-label hidden_label']) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
