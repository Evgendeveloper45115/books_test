<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Клиенты/сотрудники';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= \kartik\grid\GridView::widget([
        'id' => 'user-index',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'email',
                'format' => 'raw',
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'placeholder' => 'Email'
                ],

            ],
            'first_name',
            'last_name',
            'p_series',
            'p_number',
            [
                'attribute' => 'role',
                'value' => function (\app\models\User $model) {
                    return \app\models\User::$roles[$model->role];
                },
                'filter' => \app\models\User::$roles
            ],
            [
                'attribute' => 'position',
                'value' => function (\app\models\User $model) {
                    return \app\models\User::$positions[$model->position];
                },
                'filter' => \app\models\User::$positions

            ],
            [
                'attribute' => 'have_book',
                'label' => 'Есть ли книга',
                'value' => function (\app\models\User $model) {
                    return $model->books ? 'Да' : 'Нет';
                },
                'filter' => [1 => 'Да', 2 => 'Нет']

            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '<span style="text-wrap: none">{update}{delete}{add}</span>',
                'header' => 'Действия',
                'buttons' => [
                    'add' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-plus"></span>', \yii\helpers\Url::to(['user-books', 'id' => $model->id]), [
                            'title' => 'Добавить книгу'
                        ]);
                    },
                ]

            ]
            ,
        ], // check the configuration for grid columns by clicking button above
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true, // pjax is set to always true for this demo
        // set your toolbar
        'toolbar' => [
            [
                'content' => '<a href="' . \yii\helpers\Url::to(['create']) . '" class="btn btn-default" title="Создать новую группу">Создать</a>',
                'options' => ['class' => 'btn-group mr-2']
            ],
            '{export}',
        ],
        'toggleDataContainer' => ['class' => 'btn-group mr-2'],
        // set export properties
        'export' => [
            'fontAwesome' => true
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'pjaxSettings' => [
            'options' => [
                'timeout' => '50000'
            ]
        ],
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'panel' => [
            'type' => \kartik\grid\GridView::TYPE_PRIMARY,
            'heading' => 'Список новых участников',
            'before' => '',
            'after' => false,
            'showFooter' => false
        ],
        'persistResize' => false,
        'toggleDataOptions' => ['minCount' => 1],
        'itemLabelSingle' => 'Участник',
        'itemLabelPlural' => 'Участников'
    ]) ?>

</div>
