<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%bookType}}`.
 */
class m210623_150005_create_bookType_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%bookType}}', [
            'id' => $this->primaryKey(),
            'type' => $this->string(255),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%bookType}}');
    }
}
