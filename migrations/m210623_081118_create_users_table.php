<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%users}}`.
 */
class m210623_081118_create_users_table extends Migration
{
    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function safeUp()
    {
        $this->createTable('{{%users}}', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(255),
            'last_name' => $this->string(255),
            'patronymic' => $this->string(255),
            'email' => $this->string(255),
            'password' => $this->string(255)->defaultValue(123456),
            'role' => $this->integer(1)->defaultValue(0),
            'position' => $this->integer(1)->defaultValue(null),
            'p_series' => $this->integer(4)->defaultValue(null),
            'p_number' => $this->integer(6)->defaultValue(null),
            'date_create' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->insert('users', [
            'first_name' => 'Админ',
            'last_name' => 'Админов',
            'patronymic' => 'Админович',
            'email' => 'admin@example.com',
            'password' => '123456',
            'role' => \app\models\User::ROLE_ADMIN
        ]);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%users}}');
    }
}
