<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%userHasBook}}`.
 */
class m210623_142454_create_userHasBook_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%userHasBook}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(11),
            'book_id' => $this->integer(11),
            'change_status' => $this->integer(1),
            'clean_status' => $this->integer(2),
            'change_date' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'date_continue' => $this->timestamp(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%userHasBook}}');
    }
}
