(function ($) {
    $(document).find('.hidden_label').each(function () {
        $(this).attr('style', 'display:none')
    });
    const showHide = (select = false) => {
        $(document).find('.hidden_input').each(function () {
            if ($(this).attr('data-role') === select) {
                $(this).prev().attr('style', null);
                $(this).attr('type', 'text');
                $(this).attr('style', null);
            } else {
                $(this).attr('type', 'hidden');
                $(this).prev().attr('style', 'display:none');
            }
        });
    };

    if ($('.choose-role-js').val() !== '') {
        showHide($('.choose-role-js').val());
    }
    $('#user-role').on('change', function () {
        let selectVal = $(this).val();
        showHide(selectVal);
    });
    $(document).on("click", '.js-take-button', function (e) {
        e.preventDefault();
        $.ajax({
            url: '/user/user-books?id=' + $(this).attr('data-user-id'),
            type: 'post',
            data: {
                book_id: $(this).attr('data-book-id'), clean_status: $('.remove-book').val()
            },
            success: function () {
                $.pjax.reload({container:'#p0'});
            }
        });

    });

})(jQuery);