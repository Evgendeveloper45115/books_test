<?php

namespace app\controllers;

use app\models\Book;
use app\models\BookType;
use app\models\search\BookSearch;
use app\models\UserHasBook;
use Yii;
use app\models\User;
use app\models\search\UserSearch;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionUserBooks($id)
    {
        $user = $this->findModel($id);

        $searchModel = new BookSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->isAjax) {
            return $this->ajaxAttributes($user->id);
        }
        $UHB = $this->findExistBooks($user->id);

        if ($UHB->load(Yii::$app->request->post()) && $UHB->save()) {
            Yii::$app->session->setFlash('success', 'Успешно!');
            return $this->redirect(['user-books', 'id' => $id]);
        }

        return $this->render('user-books', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $user,
            'UHB' => $UHB,
            'books' => ArrayHelper::map($this->getNotIdBooks($user), 'id', 'title'),
            'cleanStatuses' => ArrayHelper::map(BookType::find()->all(), 'id', 'type'),
        ]);
    }

    protected function getNotIdBooks(User $user)
    {
        $not_book_id = ArrayHelper::map($user->books, 'id', 'book_id');
        return Book::find()->where(['not in', 'id', $not_book_id])->all();
    }

    protected function ajaxAttributes($user_id)
    {
        /* @var $UHB UserHasBook */
        $UHB = UserHasBook::find()->where(['book_id' => Yii::$app->request->post('book_id'), 'user_id' => $user_id])->one();
        if ($UHB) {
            $UHB->clean_status = Yii::$app->request->post('clean_status');
            $UHB->change_status = UserHasBook::STATUS_TAKE;
            $UHB->change_date = new Expression('now()');
            return $UHB->save(false);
        }
    }

    protected function findExistBooks($user_id)
    {
        $UHB = UserHasBook::find()->where(['book_id' => Yii::$app->request->post('UserHasBook')['book_id'], 'user_id' => $user_id])->one();
        if (!$UHB) {
            $UHB = new UserHasBook();
        }
        return $UHB;
    }
}
